# CppBaseSetup

Base setup for cpp project with cmake

## getting started

### command line

```bash
mkdir build && cd build
cmake ..
make
```

### vscode

cmd + shft + B
